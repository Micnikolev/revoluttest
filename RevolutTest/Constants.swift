//
//  Constants.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

final class Constants {
    
    static let apiBase = "https://revolut.duckdns.org/"
    
    struct Server {
        static var baseParams: RequestParams {
            let baseParams = [String:String]()

            return baseParams
        }
    }
}
