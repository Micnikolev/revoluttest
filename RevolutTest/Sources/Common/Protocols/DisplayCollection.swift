//
//  DisplayCollection.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

protocol DisplayCollection {
    var numberOfSections: Int { get }
    static var modelsForRegistration: [CellViewAnyModelType.Type] { get }
    
    func numberOfRows(in section: Int) -> Int
    func model(for indexPath: IndexPath) -> CellViewAnyModelType
}

extension DisplayCollection {
    var numberOfSections: Int {
        return 1
    }
}
