//
//  TemplateEntity.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

protocol TemplateEntity {
    static var templateEntity: Self { get }
}
