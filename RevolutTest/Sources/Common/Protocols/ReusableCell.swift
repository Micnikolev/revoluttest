//
//  ReusableCell.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit.UINib

protocol ReusableCell {
    static var identifier: String { get }
    static var nib: UINib { get }
}
