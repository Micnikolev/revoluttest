//
//  Dictionary+HTTPBody.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

extension Dictionary where Key: ExpressibleByStringLiteral, Value: ExpressibleByStringLiteral {
    
    var httpQuery: Data {
        var httpQuery: [String] = []
        self.forEach { key, value in
            let keyString = "\(key)".urlEncoding
            let valueString = "\(value)".urlEncoding
            
            httpQuery.append(String("\(keyString)=\(valueString)"))
        }
        return httpQuery.joined(separator: "&").data(using: .utf8)!
    }
}
