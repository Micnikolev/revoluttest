//
//  UITableView.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit.UITableView

extension UITableView {
    func registerNibs(from displayCollection: DisplayCollection) {
        registerNibs(fromType: type(of: displayCollection))
    }
    
    private func registerNibs(fromType displayCollectionType: DisplayCollection.Type) {
        for cellModel in displayCollectionType.modelsForRegistration {
            if let tableCellClass = cellModel.cellClass() as? UITableViewCell.Type {
                registerNib(for: tableCellClass)
            }
        }
    }
    
    func registerNib(for cellClass: UITableViewCell.Type) {
        register(cellClass.nib, forCellReuseIdentifier: cellClass.identifier)
    }
    
    func dequeueReusableCell(for indexPath: IndexPath, with model: CellViewAnyModelType) -> UITableViewCell {
        
        let cellIdentifier = String(describing: type(of: model).cellClass())
        let cell = dequeueReusableCell(withIdentifier: cellIdentifier,
                                       for: indexPath)
        
        model.updateAppearance(of: cell, in: self, at: indexPath)
        model.setupDefault(on: cell, index: indexPath.row)
        
        return cell
    }
}
