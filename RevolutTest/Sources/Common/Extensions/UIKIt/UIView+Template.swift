//
//  UIView+Template.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

import UIKit.UIView

extension UIView {
    func apply(template: Bool) {
        subviews.forEach {
            $0.apply(template: template)
        }
    }
}
