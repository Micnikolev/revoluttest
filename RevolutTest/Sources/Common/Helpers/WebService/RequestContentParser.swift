//
//  RequestContentParser.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

struct RequestContentParser<T> {
    var parseLogic: (Any) -> (T?, ServerError?)
}
