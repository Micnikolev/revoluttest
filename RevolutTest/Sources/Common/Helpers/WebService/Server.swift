//
//  Server.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

enum ServerError: Error {
    case noConnection
    case requestFailed
    case emptyResponse
    case wrongResponse
    
    var desc: String {
        switch self {
        case .requestFailed:
            return "ServerError.requestFailed"
        case .noConnection:
            return "ServerError.noConnection"
        case .emptyResponse:
            return "ServerError.emptyResponse"
        case .wrongResponse:
            return "ServerError.wrongResponse"
        }
    }
}

class Server {
    let apiBase: String
    
    static var standard: Server {
        return Server(apiBase: Constants.apiBase)
    }
    
    init(apiBase: String) {
        self.apiBase = apiBase
    }
    
    func request<T: PlainObjectType>(_ request: Request<[T]>, completion: @escaping ((Data?, ServerError?) -> Void)) {
        loadRequest(request) { jsonData, error in
            guard let jsonData = jsonData else {
                completion(nil, error)
                return
            }
            
            if let jsonData = jsonData as? Data {
                completion(jsonData, nil)
            } else {
                completion(nil, .wrongResponse)
            }
        }
    }

    private func loadRequest<T>(_ request: Request<T>, completion: @escaping ((Any?, ServerError?) -> Void)) {
        guard let query = URL(string: apiBase + request.query) else {
            #if DEBUG_NETWORK_INTERACTION
                print("Session query url failed: base \(apiBase) and query \(request.query)")
            #endif
            completion(nil, .requestFailed)
            return
        }
        var sessionRequest = URLRequest(url: query)
        var params = Constants.Server.baseParams
        
        if let requestParams = request.params {
            params += requestParams
        }
        
        if request.method == .get {
            assertionFailure("Get query should not have params. Use request url for sending any parameters.")
        }
        
        sessionRequest.httpBody = params.httpQuery
        let loadSession = URLSession.shared.dataTask(with: sessionRequest) { data, _, error in
            guard error == nil else {
                #if DEBUG_NETWORK_INTERACTION
                    print("Session request error: \(String(describing: error)) for api resourse: \(request)")
                #endif
                if !Reachability.isInternetAvailable {
                    completion(nil, .noConnection)
                }
                return
            }
            guard let data = data else {
                OperationQueue.main.addOperation {
                    completion(nil, .emptyResponse)
                }
                return
            }
            
            #if DEBUG_NETWORK_INTERACTION
                let responseString = String(data: data, encoding: .utf8) ?? ""
                print("Query: \(query.absoluteString)\nResponse: \n\(responseString)\n---------")
            #endif
            
            OperationQueue.main.addOperation {
                completion(data, nil)
            }
        }
        
        loadSession.resume()
    }
}
