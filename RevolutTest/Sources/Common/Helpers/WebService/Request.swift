//
//  Request.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

typealias RequestParams = [String: String]

enum RequestMethod: String {
    case get
    case post
    case head
    case delete
    
    var string: String {
        return self.rawValue.uppercased()
    }
}

struct Request<T> {
    
    let query: String
    let params: RequestParams?
    let method: RequestMethod
    
    let parser: RequestContentParser<T>?
    
    let contentType = T.Type.self
    
    init(query: String,
         method: RequestMethod = .post,
         params: RequestParams? = nil,
         parser: RequestContentParser<T>? = nil) {
        self.query = query
        self.params = params
        self.method = method
        self.parser = parser
    }
}
