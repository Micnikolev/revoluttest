//
//  FetchingElementsProtocol.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

protocol FetchingElements {
    associatedtype Value = PlainObjectType
    
    static func fetchElements(request: Request<[Value]>, completion: @escaping ((_ list: [CurrencyEntity]) -> Void))
}
