//
//  CurrencyFetching.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

struct CurrencyFetching: FetchingElements {
    static func fetchElements(request: Request<[RatesPlainObject]>, completion: @escaping ((_ list: [CurrencyEntity]) -> Void)) {
        Server.standard.request(request, completion: { data, error in
            var currencies = [CurrencyEntity]()
            
            defer {
                DispatchQueue.main.async {
                    completion(currencies)
                }
            }
            
            guard let data = data, error == nil else { return }
            
            if let parsedCurriences = CurrencyParser.parseCurrencies(with: data) {
                currencies = parsedCurriences
            }
        })
    }
}
