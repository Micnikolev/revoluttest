//
//  CurrencyParser.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

class CurrencyParser {
    static func parseCurrencies(with data: Data) -> [CurrencyEntity]? {
        do {
            let ratesObject = try JSONDecoder().decode(RatesPlainObject.self, from: data)
            var entities = [CurrencyEntity]()
            ratesObject.rates.forEach { currency in
                let entity = CurrencyEntity(title: currency.key)
                    entity.currentCurrency = currency.value
                    entities.append(entity)
            }
            return entities
        } catch  {
            print(error)
            return nil
        }
    }
}
