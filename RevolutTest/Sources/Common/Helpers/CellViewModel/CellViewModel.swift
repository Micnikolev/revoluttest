//
//  CellViewModel.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

protocol CellViewAnyModelType {
    static func cellClass() -> UIView.Type
    func setupDefault(on cell: UIView, index: Int)
    
    func updateAppearance(of view: UIView, in parentView: UIView, at indexPath: IndexPath)
}

extension CellViewAnyModelType {
    func updateAppearance(of view: UIView, in parentView: UIView, at indexPath: IndexPath) { }
}

protocol CellViewModelType: CellViewAnyModelType {
    associatedtype CellClass: UIView
    func setup(on cell: CellClass, index: Int)
}

protocol TemplatableCellViewModelType: CellViewModelType {
    associatedtype TemplateValue: TemplateEntity
    var entity: TemplateValue { get }
}

extension TemplatableCellViewModelType {
    func setupDefault(on cell: UIView, index: Int) {
        setup(on: cell as! Self.CellClass, index: index)
    }
}

extension CellViewModelType {
    static func cellClass() -> UIView.Type {
        return Self.CellClass.self
    }
    
    func setupDefault(on cell: UIView, index: Int) {
        setup(on: cell as! Self.CellClass, index: index)
    }
}
