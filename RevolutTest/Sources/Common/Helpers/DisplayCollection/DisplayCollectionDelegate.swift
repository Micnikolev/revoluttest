//
//  DisplayCollectionDelegate.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

protocol DisplayCollectionDelegate: class {
    func updateUI()
    func pauseFetching()
    func resumeFetching()
}

extension CurrencyVC: DisplayCollectionDelegate {
    func updateUI() {
        tableView.reloadData()
    }
    
    func resumeFetching() {
        startFetch()
    }
    
    func pauseFetching() {
        timer.invalidate()
    }
}

