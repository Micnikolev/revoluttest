//
//  CurrencyEntity.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

final class CurrencyEntity: TemplateEntity {
    var title: String!
    var currentCurrency: Double = 1
    var indexInTable: Int! = 0
    
    init(title: String) {
        self.title = title
    }
}

extension CurrencyEntity {
    static var templateEntity: CurrencyEntity {
        let entity = CurrencyEntity(title: "EUR")
        return entity
    }
}
