//
//  RatesPlainObject.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 01.02.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import Foundation

struct RatesPlainObject: PlainObjectType {
    let base: String
    let date: String
    let rates: [String: Double]
    
    struct Requests {
        static func currency(with base: String) -> Request<[RatesPlainObject]> {
            return Request(query: "latest?base=\(base)")
        }
    }
}
