//
//  CurrencyTableViewCell.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var currencyTextField: UITextField!
    @IBOutlet weak var underlineView: UIView!
    
    var currencyMultiplierChanged: ((_ multiplier: Double) -> Void)!
    var currencyTextFieldBeganEditing: (() -> Void)!
    var currencyTextFieldEndEditing: (() -> Void)!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        titleLabel.textColor = UIColor.CurrencyCell.currencyTitle
        currencyTextField.textColor = UIColor.CurrencyCell.currentCurrency
        
        currencyTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func currencyTextFieldChanged(_ sender: Any) {
        guard let text = currencyTextField.text,
            let multiplier = Double(text) else { return }
        
        currencyMultiplierChanged(multiplier)
    }
    
    @IBAction func currencyTextFieldBeganEditing(_ sender: Any) {
        underlineView.backgroundColor = UIColor.CurrencyCell.currencyTextFieldHighlighted
        currencyTextFieldBeganEditing()
    }
}

extension CurrencyTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        underlineView.backgroundColor = UIColor.CurrencyCell.currencyTextFieldNormal
        textField.resignFirstResponder()
        currencyTextFieldEndEditing()
        return true
    }
}
