//
//  CurrencyTableViewCellModel.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

struct CurrencyTableViewCellModel: TemplatableCellViewModelType {
    var entity: CurrencyEntity
    var currencyMultiplier: Double
    
    var currencyMultiplierChanged: ((_ multiplier: Double) -> Void)!
    var currencyTextFieldBeganEditing: (() -> Void)!
    var currencyTextFieldEndEditing: (() -> Void)!
}

extension CurrencyTableViewCellModel {
    func setup(on cell: CurrencyTableViewCell, index: Int) {
        cell.titleLabel.text = entity.title
        cell.currencyMultiplierChanged = currencyMultiplierChanged
        cell.currencyTextFieldBeganEditing = currencyTextFieldBeganEditing
        cell.currencyTextFieldEndEditing = currencyTextFieldEndEditing
        cell.currencyTextField.text = String(entity.currentCurrency * currencyMultiplier)
        
        if index == 0 {
            cell.currencyTextField.isEnabled = true
        } else {
            cell.currencyTextField.isEnabled = false
        }
    }
}
