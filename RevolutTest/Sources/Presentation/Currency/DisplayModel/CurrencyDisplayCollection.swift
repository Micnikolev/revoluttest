//
//  CurrencyDisplayModel.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

class CurrencyDisplayCollection: DisplayCollection {
    
    weak var delegate: DisplayCollectionDelegate?
    
    public var currencies = [CurrencyEntity]()
    public var baseCurrency = "EUR"
    private var currencyMultiplier: Double = 1.0

    static var modelsForRegistration: [CellViewAnyModelType.Type] {
        return [CurrencyTableViewCellModel.self]
    }
    
    func updateCurrencySection(with currencies: [CurrencyEntity]) {
        self.currencies = []
        self.currencies.append(CurrencyEntity(title: baseCurrency))
        self.currencies += currencies
        delegate?.updateUI()
    }
    
    func numberOfRows(in section: Int) -> Int {
        return currencies.count
    }
    
    func model(for indexPath: IndexPath) -> CellViewAnyModelType {
        return CurrencyTableViewCellModel(entity: currencies[indexPath.row], currencyMultiplier: currencyMultiplier,
                                          
            currencyMultiplierChanged: {
                [weak self] multiplier in
                self?.currencyMultiplier = multiplier
            },
            
            currencyTextFieldBeganEditing: {
                [weak self] in
                self?.delegate?.pauseFetching()
            },
            
            currencyTextFieldEndEditing: {
                [weak self] in
                self?.delegate?.resumeFetching()
            })
    }
    
    func didSelectCell(at indexPath: IndexPath) {
        baseCurrency = currencies[indexPath.row].title
    }
}
