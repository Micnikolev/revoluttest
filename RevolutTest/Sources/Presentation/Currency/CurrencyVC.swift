//
//  CurrencyVC.swift
//  RevolutTest
//
//  Created by Michael Nikolaev on 29.01.2018.
//  Copyright © 2018 Michael Nikolaev. All rights reserved.
//

import UIKit

class CurrencyVC: UITableViewController {

    var displayCollection: CurrencyDisplayCollection!
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        displayCollection = CurrencyDisplayCollection()
        displayCollection.delegate = self
        
        tableView.registerNibs(from: displayCollection)
        
        self.startFetch()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        timer.invalidate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return displayCollection.numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayCollection.numberOfRows(in: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = displayCollection.model(for: indexPath)
        let cell = tableView.dequeueReusableCell(for: indexPath, with: model)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        displayCollection.didSelectCell(at: indexPath)
    }

}

extension CurrencyVC {
    
    /// Description: Fetching and reloading current currencies every 1 second
    func startFetch() {
        timer = Timer(timeInterval: 1, target: self, selector: #selector(fetchCurrencies), userInfo: nil, repeats: true)
        RunLoop.current.add(timer, forMode: .commonModes)
    }
    
    @objc func fetchCurrencies() {
        CurrencyFetching.fetchElements(request: RatesPlainObject.Requests.currency(with: displayCollection.baseCurrency), completion: {
            [weak self] currencies in
            self?.displayCollection.updateCurrencySection(with: currencies)
            self?.tableView.reloadData()
        })
    }
}

